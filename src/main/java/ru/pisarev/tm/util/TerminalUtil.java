package ru.pisarev.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

    static void incorrectValue() {
        System.out.println("Incorrect values");
    }

}
