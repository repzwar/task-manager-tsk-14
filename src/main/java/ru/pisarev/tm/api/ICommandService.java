package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
