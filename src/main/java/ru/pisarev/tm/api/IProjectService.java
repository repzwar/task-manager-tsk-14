package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project add(Project project);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    void clear();

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

}
