package ru.pisarev.tm.service;

import ru.pisarev.tm.api.ICommandRepository;
import ru.pisarev.tm.api.ICommandService;
import ru.pisarev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
