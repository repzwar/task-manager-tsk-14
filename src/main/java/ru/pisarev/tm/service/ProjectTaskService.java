package ru.pisarev.tm.service;

import ru.pisarev.tm.api.IProjectRepository;
import ru.pisarev.tm.api.IProjectTaskService;
import ru.pisarev.tm.api.ITaskRepository;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;


    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.bindTaskToProjectById(taskId, projectId);
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        String projectId = projectRepository.findByIndex(index).getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        String projectId = projectRepository.findByName(name).getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}