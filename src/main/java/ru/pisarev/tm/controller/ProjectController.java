package ru.pisarev.tm.controller;

import ru.pisarev.tm.api.IProjectService;
import ru.pisarev.tm.api.IProjectTaskService;
import ru.pisarev.tm.enumerated.Sort;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.pisarev.tm.util.TerminalUtil.incorrectValue;

public class ProjectController implements ru.pisarev.tm.api.IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }

    @Override
    public void showById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            incorrectValue();
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    private void showProject(Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @Override
    public void create() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project project = add(name, description);
        if (project == null) {
            System.out.println("Incorrect  values");
            return;
        }
        projectService.add(project);
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return new Project(name, description);
    }

    @Override
    public void clear() {
        projectService.clear();
    }

    @Override
    public void removeById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            incorrectValue();
            return;
        }
        projectTaskService.removeProjectById(id);
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            incorrectValue();
            return;
        }
        projectTaskService.removeProjectByIndex(index);
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            incorrectValue();
            return;
        }
        projectTaskService.removeProjectByName(name);
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            incorrectValue();
            return;
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated == null) incorrectValue();
    }

    @Override
    public void updateById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            incorrectValue();
            return;
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated == null) incorrectValue();
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) incorrectValue();
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) incorrectValue();
    }

    @Override
    public void startByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) incorrectValue();
    }

    @Override
    public void finishById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) incorrectValue();
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) incorrectValue();
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) incorrectValue();
    }

}
