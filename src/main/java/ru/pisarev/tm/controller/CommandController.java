package ru.pisarev.tm.controller;

import ru.pisarev.tm.api.ICommandController;
import ru.pisarev.tm.api.ICommandService;
import ru.pisarev.tm.model.Command;
import ru.pisarev.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHelp() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void displayCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name != null) System.out.println(name);
        }
    }

    @Override
    public void displayArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String arg = command.getArg();
            if (arg != null) System.out.println(arg);
        }
    }

    @Override
    public void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    @Override
    public void displayVersion() {
        System.out.println("0.9.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("Aleksey Pisarev");
        System.out.println("pisarevaleks@mail.ru");
    }

    @Override
    public void displayInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
